
#ifndef LINKED_LIST__H
#define LINKED_LIST__H

#include <stdlib.h>
#include <stdio.h>

typedef struct ListNode_t {
  struct ListNode_t *prev, *next;
  void *data;
} ListNode;

typedef enum {
  INT, DOUBLE, FLOAT
} DATA_TYPE;


typedef struct List {
  ListNode *head, *tail;
  int count;
} List;

List *init_list();

void append(List *list, void *data);

void print_list(List *list, DATA_TYPE t);

#endif
