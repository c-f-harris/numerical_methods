
#include "linked_list.h"

List *init_list()
{
    List *list = (List *)malloc(sizeof(List));
    list->count = 0;
    list->head = (ListNode *)malloc(sizeof(ListNode));
    list->tail = (ListNode *)malloc(sizeof(ListNode));
    list->head->next = list->tail;
    list->head->prev = NULL;
    list->tail->prev = list->head;
    list->tail->next = NULL;
    return list;
}

void append(List *list, void *data)
{
    ListNode *n = (ListNode *)malloc(sizeof(ListNode));
    n->prev = list->tail->prev;
    n->next = list->tail;
    list->tail->prev->next = n;
    list->tail->prev = n;
    n->data = data;
}

void print_list(List *list, DATA_TYPE t)
{
    ListNode *walker = list->head->next;
    while(walker != list->tail) {

      if(t==INT) printf("%d", *(int *)walker->data);
      else if(t == DOUBLE) printf("%.3f", *(double *)walker->data);
      else if(t == FLOAT) printf("%.3f", *(float *)walker->data);

      walker = walker->next;
      if(walker != list->tail) printf(", ");
      else printf("\n");

    }
}
