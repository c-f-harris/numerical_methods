
#ifndef __MATHUTIL
#define __MATHUTIL

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

#define MAX_FLOAT_LENGTH 100

typedef struct Array {
  double **X;
  int rows, cols;
} Array;

Array *zeros(int rows, int cols);
Array *eye(int n);
Array *ones(int rows, int cols);
Array *transpose(Array *A);
Array *random_array(int rows, int cols, double max_entry);
//Array *array_copy(Array *A);
double random_value(double a, double b);

void set_at(Array **A, int row, int col, double val);
double at(Array *A, int row, int col);

Array *dot(Array *A, Array *B); //todo
Array *multiply(Array *A, Array *B);
bool inverse(Array *A, Array **inverse);
double norm(Array *A); // Returns Euclidean norm if vector, returns Frobenius norm if matrix
double norm_diff(Array *A, Array *B);

//==================
//    Matrix I/O
//==================
// Returns 1 when expected length is true.
int split_by_comma(char *str, int expected_len, double *X);
Array *matrix_from_file(char *filepath); // Returns NULL on failure.

//==================
//      Debug
//==================
void print_array(Array *A);
void print_invalid_textfile_error(char *filepath);


#endif // __MATHUTIL
