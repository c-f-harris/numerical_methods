#include "mathutil.h"

void set_at(Array **A, int row, int col, double val)
{
    (*A)->X[row][col] = val;
}

double at(Array *A, int row, int col)
{
    return A->X[row][col];
}

Array *zeros(int rows, int cols)
{
  Array *A = (Array *)malloc(sizeof(Array));
  double **X = (double **)malloc(sizeof(double) * rows);
  int i, j;
  for(i = 0; i < rows; i++)
  {
    *(X + i) = (double *)malloc(sizeof(double) * cols);
    for(j = 0; j < cols; j++)
    {
      *(*(X + i) + j) = 0;
    }
  }
  A->X = X;
  A->rows = rows;
  A->cols = cols;
  return A;
}

Array *eye(int n)
{
  Array *I = zeros(n,n);
  int i;
  for(i = 0; i < n; i++)
  {
    set_at(&I, i, i, 1);
  }
  return I;
}

Array *ones(int rows, int cols)
{
  Array *A = zeros(rows,cols);
  int i, j;
  for(i = 0; i < rows; i++)
  {
    for(j = 0; j < cols; j++)
    {
      set_at(&A, i, j, 1);
    }
  }
  return A;
}

Array *transpose(Array *A)
{
  Array *T = zeros(A->cols, A->rows);
  int i,j;
  for(i = 0; i < T->rows; i++)
  {
      for(j = 0; j < T->cols; j++)
      {
        set_at(&T, i, j, at(A, j, i) );
      }
  }
  return T;
}

/*
Array *array_copy(Array *B)
{
    Array *A = (Array *)malloc(sizeof(Array));
    double **Y = (double **)malloc(sizeof(double) * B->rows);
    memcpy(Y, B->X, sizeof(double)*B->rows*B->cols);

    A->X = Y;
    A->rows = B->rows;
    A->cols = B->cols;

    print_array(A);

    return A;

}
*/

//=======================================
//              RANDOM
//=======================================

Array *random_array(int rows, int cols, double max_entry)
{
  srand(3);
  Array *A = zeros(rows, cols);
  int i,j;
  for(i = 0; i < rows; i++)
  {
    for(j = 0; j < cols; j++)
    {
      set_at(&A, i, j, random_value(0, max_entry));
    }
  }
  return A;
}

double random_value(double a, double b)
{
  int val, d1, d2;
  val = rand() % (int)b;
  d1 = rand() + 1; d2 = rand();
  return a + (double)val + ((double)fmin(d1,d2) / (double)fmax(d1,d2));
}

//=======================================
//      MULTIPLICATION FUNCTIONS
//=======================================

Array *dot(Array *A, Array *B)
{
  if(A->cols != B->rows) {
      printf("\nInvalid matrix multiplication: cols(A) != rows(B)\n\n");
	    return NULL;
  }

	Array *C = zeros(A->rows, B->cols);

  int i, j, k;
  double e;
  for(i = 0; i < A->rows; i++) //foreach row of A
  {
      for(j = 0; j < B->cols; j++) //foreach col of B
      {
          // C[i,j] = dot(A(row i), B(col j))
          e = 0;
          for(k = 0; k < A->cols; k++)
          {
              e += at(A, i, k) * at(B, k, j);
          }
          set_at(&C, i, j, e);
      }
  }
  return C;
}

Array *multiply(Array *A, Array *B)
{
  if(A->rows != B->rows || A->cols != B->cols) return NULL;
  Array *C = zeros(A->rows, B->cols);
  int i,j;
  for(i = 0; i < A->rows; i++)
  {
    for(j = 0; j < B->cols; j++)
    {
      set_at(&C, i, j, at(A, i, j) * at(B, i, j));
    }
  }
  return C;
}

//=======================================
//          MATRIX IO
//=======================================
/*
int split_by_comma(double *X, const int expected_len, char *str)
{
    char num[MAX_FLOAT_LENGTH];
    int i, str_idx, num_str_idx;
    str_idx = 0;
    for(i = 0; i < expected_len; i++)  // foreach expected number
    {
        strcpy(num, "");
        num_str_idx = 0;
        while(num_str_idx < MAX_FLOAT_LENGTH && str[str_idx] != ','
            && str[str_idx] != '\0' && str[str_idx] != '\n')
        {
          num[num_str_idx] = str[str_idx];
          num_str_idx++; str_idx++;
        }

        *(X + i) = atoi(num);
        if(str[str_idx] == '\0' || str[str_idx] == '\n')
        {
          if(i == expected_len-1) return 1;
          else return 0; // not enough numbers
        }
        else str_idx++; // skip comma
    }
    return 1;
}
*/

int split_by_comma(char *str, int expected_len, double *X)
{
    char buffer[MAX_FLOAT_LENGTH];
    int str_idx, buffer_idx, num_idx;
    int string_length;

    str_idx = 0;
    num_idx = 0;
    string_length = strlen(str);

    while(str[str_idx] != '\0' && str_idx < string_length && num_idx < expected_len)
    {
        buffer_idx = 0;
        while(str[str_idx] != '\0' && str[str_idx] != ',' && buffer_idx < MAX_FLOAT_LENGTH)
        {
            buffer[buffer_idx++] = str[str_idx++];
        }
        if(str[str_idx] == ',') str_idx++; // skip comma
        buffer[buffer_idx] = '\0';

        *(X + num_idx++) = atof(buffer);
    }

    return num_idx == expected_len;
}

Array *matrix_from_file(char *filepath)
{
  int rows,cols;
  int i,j;

  FILE *file = fopen(filepath, "r");
  if(file == NULL)
  {
    printf("\nInvalid filepath:%s\n\n", filepath);
    return NULL;
  }

  // Get dimensions:
  char row_str[30];
  char col_str[30];
  fgets(row_str, 30, file);
  fgets(col_str, 30, file);
  rows = atoi(row_str);
  cols = atoi(col_str);
  const int MAX_STR_LEN = cols * MAX_FLOAT_LENGTH;
  char entry_str[MAX_STR_LEN];

  double *R = (double *)malloc(cols * sizeof(double));
  Array *A = zeros(rows, cols);

  //Read matrix:
  for(i = 0; i < rows; i++)
  {
      // Get Row:
      if( fgets(entry_str, MAX_STR_LEN, file) == NULL )
      {
          print_invalid_textfile_error(filepath);
          return NULL;
      }

      if( split_by_comma(entry_str, cols, R) == 0 )
      {
          print_invalid_textfile_error(filepath);
          return NULL;
      }

      for(j = 0; j < cols; j++)
      {
          set_at( &A, i, j, R[j] );
      }

  }

  return A;
}

//=======================================
//          DEBUG FUNCTIONS
//=======================================

void print_invalid_textfile_error(char *filepath)
{
    printf("\nInvalid text file format in file %s.\n\n", filepath);
}

void print_array(Array *A)
{
    int i,j;
    printf("\n");
    for(i = 0; i < A->rows; i++)
    {
      for(j = 0; j < A->cols; j++)
      {
        printf("%.6f ", at(A, i, j));
      }
      printf("\n");
    }
    printf("\n");
}



//=========================================================================================
//                              Linear Algebra
//=========================================================================================

// These functions are from geeksforgeeks.com for finding inverse

// Function to get cofactor of A[p][q] in temp[][]. n is current
// dimension of A[][]
void get_cofactor(Array *A, Array **temp, int p, int q, int n)
{
    int i = 0, j = 0;
    int row, col;
    // Looping for each element of the matrix
    for (row = 0; row < n; row++)
    {
        for (col = 0; col < n; col++)
        {
            //  Copying into temporary matrix only those element
            //  which are not in given row and column
            if (row != p && col != q)
            {
                set_at(temp, i, j++, at(A, row, col));
                // Row is filled, so increase row index and
                // reset col index
                if (j == n - 1)
                {
                    j = 0;
                    i++;
                }
            }
        }
    }
}

/* Recursive function for finding determinant of matrix.
   n is current dimension of A[][]. */
int determinant(Array *A, int n)
{
    int D = 0; // Initialize result

    //  Base case : if matrix contains single element
    if (n == 1) return at(A, 0, 0);

    Array *temp = zeros(A->rows,A->rows); // To store cofactors
    int sign = 1;  // To store sign multiplier

     // Iterate for each element of first row
     int f;
     for (int f = 0; f < n; f++)
     {
          // Getting Cofactor of A[0][f]
          get_cofactor(A, &temp, 0, f, n);

          D += sign * at(A,0,f) * determinant(temp, n - 1);

          // terms are to be added with alternate sign
          sign = -sign;
      }
      return D;
}

// Function to get adjoint of A[N][N] in adj[N][N].
void adjoint(Array *A, Array **adj)
{
    if (A->rows == 1)
    {
        set_at(adj, 0, 0, 1);
        return;
    }

    // temp is used to store cofactors of A[][]
    int sign = 1;
    Array *temp = zeros(A->rows, A->rows);

    int i, j;
    for (i = 0; i < A->rows; i++)
    {
        for (j = 0; j < A->rows; j++)
        {
            // Get cofactor of A[i][j]
            get_cofactor(A, &temp, i, j, A->rows);

            // sign of adj[j][i] positive if sum of row
            // and column indexes is even.
            sign = ((i+j)%2==0)? 1: -1;

            // Interchanging rows and columns to get the
            // transpose of the cofactor matrix
            set_at(adj, j, i, (sign)*(determinant(temp, A->rows-1)));
            //adj[j][i] = (sign)*(determinant(temp, N-1));
        }
    }
}

// Function to calculate and store inverse, returns false if
// matrix is singular
bool inverse(Array *A, Array **inverse)
{
    // Find determinant of A[][]
    int det = determinant(A, A->rows);

    if (det == 0)
    {
        printf("\nERROR: Singular matrix passed into inverse(...)\n\n");
        return false;
    }

    // Find adjoint
    Array *adj = zeros(A->rows, A->rows);
    adjoint(A, &adj);

    // Find Inverse using formula "inverse(A) = adj(A)/det(A)"
    int i, j;
    for (i = 0; i < A->rows; i++)
    {
        for (j = 0; j < A->rows; j++)
        {
            set_at(inverse, i, j, (at(adj, i, j) / (float)det));
        }
    }
    return true;
}

double norm(Array *A)
{
    printf("\nNORM\n\n");
    int i,j;
    double n = 0, x;
    for(i = 0; i < A->rows; i++)  {
      for(j = 0; j < A->cols; j++) {
        x = at(A, i,j);
        n += x*x;
      }
    }
    return pow(n, 0.5);
}

double norm_diff(Array *A, Array *B)
{
    if(A->rows != B->rows || A->cols != B->cols) {
      printf("Invalid norm_diff: A shape not equal B shape.");
      return 0;
    }

    printf("\nNORM DIFF\n\n");
    Array *C = zeros(A->rows, A->cols);

    int i,j;
    for(i = 0; i < A->rows; i++) {
      for(j = 0; j < A->cols; j++) {

          printf("A = \n");
          print_array(A);

          printf("\nat Bij = %.5f\n", at(B,i,j));
          printf("\nat Aij = %.5f\n", at(A,i,j));

          set_at(&C, i, j, at(A,i,j) - at(B,i,j));
          print_array(C);

      }
    }
    return norm(C);
}


//
