
#ifndef __LEASTSQUARES
#define __LEASTSQUARES

#include "mathutil.h"

Array *least_squares(Array *A, Array *Y);

#endif // __LEASTSQUARES
