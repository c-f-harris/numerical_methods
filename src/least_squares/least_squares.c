

#include "least_squares.h"


Array *least_squares(Array *A, Array *Y)
{
  Array *T = transpose(A);
  Array *M = dot(T, A);
  Array *inv = zeros(M->rows, M->rows);
  bool nonsingular = inverse(M, &inv);

  if(!nonsingular) {
      printf("\nERROR: A^t dot A is singular\n\n");
      return NULL;
  }

  Array *xhat;
  xhat = dot(inv, T);
  xhat = dot(xhat, Y);
  return xhat;
}
