#ifndef BISECTION_H
#define BISECTION_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "linked_list.h"

#define INT_MAX 2147483647

double bisection( double A, double B, double (*f)(double), double eps, int MAX_N );

#endif
