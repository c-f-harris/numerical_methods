
#include "bisection.h"

double bisection( double A, double B, double (*f)(double), double eps, int MAX_N )
{

    if((*f)(A) >= 0) {  printf("Invalid. A >= 0."); return INT_MAX;   }
    if((*f)(B) <= 0) {  printf("Invalid. B <= 0."); return INT_MAX;   }

    double C = 0, prev_C = INT_MAX;
    double fC;
    int iter = 0;

    while (fabs(C - prev_C) > eps && iter <= MAX_N) {

        printf("C = %.5f\n", C);
        prev_C = C;
        C = (A+B)/2.0;
        fC = (*f)(C);
        if(fC < 0) A = C;
        else if(fC >= 0) B = C;
        iter++;

    }

    if(iter>=MAX_N) {
        printf("\n\nMAX ITERATIONS REACHED WITH NO ROOT DISCOVERED.\n\n");
        return INT_MAX;
    }

    return C;
}
