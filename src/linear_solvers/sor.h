
#ifndef SOR_H
#define SOR_H

#include "../mathutil/mathutil.h"

Array *sor_solve(Array *A, Array *b, Array *x0, double omega, double eps, int max_iter);

#endif
