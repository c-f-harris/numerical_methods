
#include "sor.h"

Array *sor_solve(Array *A, Array *b, Array *x, double omega, double eps, int max_iter)
{
    if(A->rows != A->cols)
    {
        printf("ERROR. System of equations NOT n by n.");
        return x;
    }

    int iter, i, j, N;
    double tmp;
    double x_prev, norm_difference;

    iter = 0;
    N = A->rows;

    do
    {
        norm_difference = 0;

        for(i = 0; i < N; i++)
        {
            x_prev = at(x, i, 0);
            tmp = at(b, i, 0);
            for(j = 0; j < N; j++) tmp -= at(A,i,j)*at(x,j,0);
            tmp *= (omega / at(A,i,i));
            tmp = at(x,i,0) + tmp;

            set_at(&x, i, 0, tmp);
            norm_difference += fabs(x_prev - tmp);
        }

        iter++;

    } while(norm_difference > eps && iter < max_iter);

    if(iter >= max_iter)
    {
        printf("\nSOR did not converge after %d iterations.\n\n", max_iter);
    }

    return x;
}
