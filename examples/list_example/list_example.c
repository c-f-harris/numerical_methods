
#include "../../src/list/linked_list.h"

int main()
{

  List *l = init_list();

  printf("Real data:\n\n");
  int i;
  double *d;
  for(i = 0; i < 10; i++) {
      d = (double *)malloc(sizeof(double));
      *d = ((double)(rand() % 1000)) / 100.0;
      printf("%.3f\n", *d);
      append(l, d);
  }

  printf("\nPrinting values in list...\n\n");
  print_list(l, DOUBLE);

  return 1;
}
