
#include "mathutil.h"
#include "least_squares.h"

int main()
{
    char *X_PATH = "x_ls_example_mat.txt";
    char *Y_PATH = "y_ls_example_mat.txt";

    Array *A = matrix_from_file(X_PATH);
    Array *Y = matrix_from_file(Y_PATH);
    Array *xhat = least_squares(A,Y);

    print_array(xhat);
    return 1;
}
