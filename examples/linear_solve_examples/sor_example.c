
#include <stdlib.h>
#include "mathutil.h"
#include "sor.h"

int main()
{
    Array *A = matrix_from_file("matrix1.txt");
    Array *y = matrix_from_file("y.txt");

    printf("\nA = \n");
    print_array(A);
    printf("\ny = \n");
    print_array(y);

    Array *x0 = zeros(A->rows, 1);
    double omega, eps;
    omega = 0.2;
    eps = 0.0000001;
    int max_iter = 10000;

    Array *x = sor_solve(A, y, x0, omega, eps, max_iter );
    Array *Ax = dot(A,x);

    printf("\nSOLUTION\n\nx = \n");
    print_array(x);
    printf("\nA*x = \n");
    print_array(Ax);
    printf("\n");
    return 1;
}
