
#include "../../src/nonlinear_solvers/bisection/bisection.h"
#include <math.h>

double f(double x) {
    return pow(x,3) - pow(x,2);
}

int main()
{
    double inrange[2];
    inrange[0] = -10; inrange[1] = 10;
    double A = -100, B = 110;
    double result = bisection(A, B , f, 0.0000001, 1000);
    printf("\nROOT:     %.5f\n\n", result);
    printf("f(ROOT):  %.5f\n\n", f(result));
    return 1;
}
