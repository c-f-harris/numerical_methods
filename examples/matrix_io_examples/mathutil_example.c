
#include "../../mathutil/mathutil.h"

int main()
{

    printf("\n3x3 Identity Matrix:\n");
    Array *A = eye(3);
    print_array(A);

    //===================================
    printf("\n3x4 Random Matrix:\n");
    A = random_array(3, 4, 50);
    print_array(A);

    //===================================
    printf("\nIts Transpose:\n");
    Array *A_t = transpose(A);
    print_array(A_t);

    //===================================
    printf("\nA * A^t = \n");
    Array *C = dot(A, A_t);
    print_array(C);
    printf("\n");

    //===================================
    char *INPUT_FILEPATH = "example_mat.txt";
    printf("Reading matrix from file %s:\n", INPUT_FILEPATH);
    A = matrix_from_file(INPUT_FILEPATH);
    if(A != NULL) print_array(A);
    else return 0;
    printf("\n");

    //===================================
    printf("A^T * A = \n");
    C = dot(transpose(A), A);
    print_array(C);
    printf("\n");

    return 1;
}
