This is a numerical method library written in C.

### Matrix File Format:
	m (number of rows)
	n (number of cols)
	a11,a12,...,a1n
	a21,a22,...,a2n
	...
	am1,am2,...,amn


### Approximation Algorithms
- Least Squares 

### Linear System Solvers
- SOR [TODO]
- Gaussian Elimination [TODO]
- LU Decomposition [TODO]

### Ordinary Differential Equation Solvers
- Boundary value problem (BVP) using SOR [TODO]
- Backward Euler [TODO]
- Heun's method [TODO]

### Partial Differential Equation Solvers


### Nonlinear Equation Solvers
- Newton's method single variable [TODO]
- Newton's method multivariable [TODO]
- Bisection 
